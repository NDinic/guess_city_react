import React, { Fragment } from 'react';

const title = (props) => {

  return (
    <Fragment>{props.title}</Fragment>
  )
}

export default title;