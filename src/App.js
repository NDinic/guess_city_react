import React, { Component } from 'react';
import './App.css';
import { Route } from 'react-router-dom';
import Main from './containers/Main';
import Results from './containers/Results';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="container">
          <Route path="/" exact component={Main} />
          <Route path="/results" component={Results} />
        </div>
      </div>
    )
  }
}

export default App;