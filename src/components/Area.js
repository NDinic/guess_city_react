import React from 'react';
import Title from '../shared/Title';

const area = (props) => {
  return (
    <h2 id="area"><Title title='Oblast:' /> {props.area}</h2>
  )
}

export default area;