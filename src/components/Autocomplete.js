import React from 'react';

const autocomplete = (props) => {
  const showList = () => {
    return (
      <div id="cityAutocomplete" className={props.activeL ? 'active' : null}>
        {props.filtered.map((city, i) => {
          return (
            <span onClick={props.clicked} data-val={city} key={city + i}>{city}</span>
          )
        })}
      </div>
    )
  }
  return (
    <div className="cityWrap">
      <input id="cityInput" type="text" name="city" value={props.cityVal} onChange={props.changed} onFocus={props.cityFocus} />
      {showList()}
      <button id="btnAddList" type="button" onClick={props.addToList}>Dodaj grad u listu</button>
    </div>
  )
}

export default autocomplete;