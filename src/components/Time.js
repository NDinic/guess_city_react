import React from 'react'
import Title from '../shared/Title';

let time = (props) => {
  return (
    <h2 id="areaTime"><Title title='Vreme:' /> {props.time}</h2>
  )
}

export default time;