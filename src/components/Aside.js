import React from 'react';

const Aside = (props) => {
  return (
    <aside>
      <h2>Lista ubacenih gradova</h2>
      <ul id="citiesListBlock">
        {props.listed.map((city, i) => {
          return (
            <li key={city + i} onClick={() => props.deleteCity(i)}>{city}</li>
          )
        })}
      </ul>
      <button id="endGame" type='button' onClick={props.endGame}>Zavrsi igru</button>
    </aside>
  )
}

export default Aside;