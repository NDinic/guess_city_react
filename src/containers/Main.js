import React, { Component, Fragment } from 'react';
import Autocomplete from '../components/Autocomplete';
import Aside from '../components/Aside';
import data from '../podaci.json';

import Area from '../components/Area';
import Time from '../components/Time';
import Title from '../shared/Title';
import { Redirect } from 'react-router-dom';

class Main extends Component {
  // 
  state = {
    time: 7, //data.vreme,
    addedCities: [],
    filteredList: [],
    correct: data.tacno,
    activeList: false,
    city: '',
    queryToString : null
  }

  timerHandler = () => {
    if (this.state.time !== 0) {
      this.setState((prevState) => {
        return {
          time: prevState.time - 1
        }
      })
    }
    if (this.state.time === 0) {
      // clearInterval(this.timer);
      this.endGameHandler();
    }
  }
  timer = '';
  componentDidMount() {
    this.timer = setInterval(this.timerHandler, 1000);
  }
 
  onChangeHandler = e => {
    let cityValue = e.target.value;
    this.setState({
      filteredList: data.ponudjene.filter(city => city.toLowerCase().startsWith(cityValue.toLowerCase())),
      activeList: true,
      city: cityValue
    })
  }

  cityHandleFocus = () => {
    this.setState({
      activeList: false,
      city: ''
    })
  }

  onClickHandler = (e) => {
    this.setState({
      city: e.target.dataset.val,
      activeList: false
    })
  }

  addCityListHandler = () => {
    if (this.state.city.length > 0 && !this.state.addedCities.includes(this.state.city.toLowerCase())) {
      const citiesTemporary = this.state.addedCities.slice()
      citiesTemporary.push(this.state.city.toLowerCase());
      this.setState({
        addedCities: citiesTemporary,
        city: ''
      })
    } else {
      this.setState({
        city: ''
      })
      return false;
    }
  }
  deleteCityHandler = (cityIndex) => {
    const citiesTemporary = this.state.addedCities.slice()
    citiesTemporary.splice(cityIndex, 1);
    this.setState({
      addedCities: citiesTemporary
    })
  }
  endGameHandler = () => {
    this.setState({'time': 0});
    clearInterval(this.timer);
    let correctAnswers = [];
    for (let i = 0; i < this.state.correct.length; i++) {
      for (let j = 0; j < this.state.addedCities.length; j++) {
        if (this.state.addedCities[j].toLowerCase() === this.state.correct[i].toLowerCase()) {
          correctAnswers.push(this.state.correct[i]);
        }
      }
    }

    let queryToString = 'res='
      + encodeURIComponent(this.state.addedCities)
      + '&correct='
      + encodeURIComponent(correctAnswers)
      + '&sum='
      + encodeURIComponent(correctAnswers.length);

    this.setState({ 
      'submitted': true,
      'queryToString' : queryToString
     })

    // this.props.history.push({
    //   pathname: '/results',
    //   search: queryToString
    // });
  }

  render() {
    let redirect = null;
    if (this.state.queryToString) {
      redirect = <Redirect to={'/results/?' + this.state.queryToString }/>
    }
    return (
      <Fragment>
        {redirect}
        <h1><Title title='Pogodi u kojoj se oblasti nalazi grad' /></h1>
        <main>
          <section>
            <div>
              <Area area={data.oblast} />
              <Time time={this.state.time} />
            </div>
            <Autocomplete
              changed={this.onChangeHandler}
              clicked={this.onClickHandler}
              cityFocus={this.cityHandleFocus}
              addToList={this.addCityListHandler}
              filtered={this.state.filteredList}
              activeL={this.state.activeList}
              cityVal={this.state.city}
            />
          </section>
        </main>
        <Aside
          listed={this.state.addedCities}
          deleteCity={this.deleteCityHandler}
          endGame={this.endGameHandler}
        />
      </Fragment>
    )
  }

}

export default Main;