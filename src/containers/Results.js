import React, { Component } from 'react';
import queryString from 'query-string';
import Title from '../shared/Title';
class Results extends Component {
  state = {
    correct: null,
    results: null
  }
  componentDidMount() {
    const getSearchQuery = queryString.parse(this.props.location.search);
    let resSplit = getSearchQuery.res.split(',');
    let resUpperCase = resSplit.map(r => r.charAt(0).toUpperCase() + r.substring(1))
    resUpperCase = resUpperCase.join(', ');

    this.setState({
      correct: getSearchQuery.correct,
      result: resUpperCase,
      sum: getSearchQuery.sum
    })

  }
  render() {
    return (
      <div>
        <h1><Title title='Rezultat' /></h1>
        <div>
          <h2><Title title='Broj pogodaka:' /></h2>
          <p>{this.state.sum}</p>
        </div>
        <div>
          <h2><Title title='Vasi izabrani gradovi su:' /></h2>
          <p>{this.state.result}</p>
        </div>
        <div>
          <h2><Title title='Pogodjeni gradovi su:' /></h2>
          <p>{this.state.correct}</p>
        </div>
      </div>
    )
  }
}

export default Results;
